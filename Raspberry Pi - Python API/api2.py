#before running, activate the virtual environment: source env/bin/activate
# to discover your api: ip addr show http://192.168.1.74:1234
#setting up virtual environment: https://realpython.com/python-virtual-environments-a-primer/
#code adapted from: https://www.codementor.io/sagaragarwal94/building-a-basic-restful-api-in-python-58k02xsiq


from flask import Flask
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask_sqlalchemy import SQLAlchemy
from json import dumps
from flask import jsonify
import sys

app = Flask(__name__)
api = Api(app)

for arg in sys.argv: 
    percentage = arg


class Bins(Resource):
   def get(self):     
      result = {"fill level" : percentage}
      return jsonify(result)

api.add_resource(Bins, '/bins') 

if __name__ == '__main__':
   app.run(host="0.0.0.0", port="1234")